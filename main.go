package main

//go:generate esc -o html.go -prefix html html

import (
	"flag"
	"net/http"

	"github.com/BlueMasters/firebasedb"
	log "github.com/Sirupsen/logrus"
	"github.com/cenkalti/backoff"
	"github.com/vharitonsky/iniflags"
	"gitlab.com/geomyidae/ws2811sim"
	"gitlab.com/telecom-tower/msgroller"
	"gopkg.in/jcelliott/turnpike.v2"
)

const (
	rows    = 8
	columns = 128
)

func main() {
	// Read arguments
	var firebaseUrl = flag.String("firebase-url", "https://telecom-tower.firebaseio.com/currentBitmap", "Firebase URL")
	var debug = flag.Bool("debug", false, "be verbose")
	iniflags.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	log.Infoln("Starting tower")

	// create WAMP server and client
	server := turnpike.NewBasicWebsocketServer("ws2811")
	client, err := server.GetLocalClient("ws2811", nil)
	if err != nil {
		log.Panic(err)
	}

	// create ws2811 simulator
	opt := ws2811sim.DefaultOptions
	opt.WampClient = client
	opt.LedCount = rows * columns
	opt.Frequency = 800000
	ws := ws2811sim.MakeWS2811(&opt)

	// create connection of firebase
	log.Debugln("Making firebase reference")
	backOff := backoff.NewExponentialBackOff()
	backOff.MaxElapsedTime = 0 // retry forever
	ref := firebasedb.NewReference(*firebaseUrl).Retry(backOff)
	if ref.Error != nil {
		log.Fatal(err)
	}

	// start the tower goroutines pipeline
	closer := make(chan chan error)
	msgroller.Painter(
		msgroller.Sequencer(
			msgroller.Feeder(ref, closer),
			rows,
			columns),
		ws,
		rows,
		columns)

	http.Handle("/data", server)
	http.Handle("/", http.FileServer(FS(false)))
	log.Println("Serving at localhost:8080...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
